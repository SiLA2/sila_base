# Overview of status in the **SiLA** implementations

This is a quick overview of the implementation status of different SiLA language implementations - as a fast orientation and decision help.

**Legend**

**`x`** - fully implemented  
**`x*`** - partially implemented  
**`o`** - under development  
**`-`** - not implemented yet  

## Repositories

- C++ ([sila_cpp](https://gitlab.com/SiLA2/sila_cpp))
- C# ([sila_csharp](https://gitlab.com/SiLA2/sila_csharp))
- C#-Tecan ([sila_tecan](https://gitlab.com/SiLA2/vendors/sila_tecan))
- Java ([sila_java](https://gitlab.com/SiLA2/sila_java))
- Python ([sila_python](https://gitlab.com/SiLA2/sila_python))

## SiLA Client

You can use the following repositories to generate SiLA Clients.
Also dynamic Client is supported.

|                                | C++  | C#  | C#-Tecan | Java | Python |
| ------------------------------ | ---- |---- | -------- | ---- | ------ |
|  Dynamic Client / Late Binding | o    | -   | x        | x    |      x |
|  Full Code Generator           | o    | -   | x        | x    |      x |

## SiLA Server

You can use the following repositories to generate SiLA Servers.

|                                | C++  | C#  | C#-Tecan | Java | Python |
| ------------------------------ | ---- |---- | -------- | ---- | ------ |
|  Full Code Generator           | o    | -   | x        | x    |      x |

## Connection

The implementation status of the two connection methods (client and server initiated) are the following:

|                    | C++  | C#  | C#-Tecan | Java | Python |
| ------------------ | ---- |---- | -------- | ---- | ------ |
|  Client-Initiated  | x    | x   | x        | x    |      x |
|  Server-Initiated  | -    | -   | x        | o    |      - |

## Observable and Unobservable Commands

All implementations have implemented Observable and Unobservable Commands.

## Observable and Unobservable Properties

All implementations have implemented Observable and Unobservable Properties.

## SiLA Basic Types

All implementations have implemented all SiLA Basic Types except the following:

|                                | C++  | C#  | C#-Tecan | Java | Python |
| ------------------------------ | ---- |---- | -------- | ---- | ------ |
|  Binary Type (max. 2MiB)       | -    | x   | x        | x    |      x |
|  Binary Type (Binary Transfer) | -    | x   | x        | x    |      x |
|  Any Type                      | x*   | -   | x        | o    |      x |

### SiLA Derived Data Types

|                   | C++  | C#  | C#-Tecan | Java | Python |
| ----------------- | ---- |---- | -------- | ---- | ------ |
|  List Type        | x    | x   | x        | x    |      x |
|  Structure Type   | -    | x   | x        | x    |      x |
|  Constrained Type | -    | x   | x        | o    |      x |

## SiLA Standard Features

|                                      | Category      | C++  | C#  | C#-Tecan | Java | Python |
| ------------------------------------ | ------------- | ---- |---- | -------- | ---- | ------ |
|  Authentication Service              | Core          | -    | x   | x        | x    |      x |
|  Authorization Configuration Service | Core          | -    | -   | x        | o    |      - |
|  Authorization Provider Service      | Core          | -    | -   | x        | o    |      x |
|  Authorization Service               | Core          | -    | x   | x        | x    |      x |
|  Connection Configuration Service    | Core          | -    | -   | -        | x    |      - |
|  Error Recovery Service              | Core          | -    | -   | -        | -    |      - |
|  Lock Controller                     | Core          | -    | x   | x        | x    |      x |
|  SiLA Service                        | Core          | x    | x   | x        | x    |      x |
|  Simulation Controller               | Core          | -    | x*  | -        | x    |      - |
|  Cancel Controller                   | Core          | -    | -   | -        | -    |      - |
|  Parameter Constraints Provider      | Core          | -    | -   | -        | -    |      - |
|  Pause Controller                    | Core          | -    | -   | -        | -    |      - |

## SiLA Server Discovery

All implementations support Server Discovery.

## SiLA Client Metadata 

|                            | C++  | C#  | C#-Tecan | Java | Python |
| -------------------------- | ---- |---- | -------- | ---- | ------ |
|  SiLA Client Metadata      | -    | x   | x        | x    |      x |

## Error Categories

|                            | C++  | C#  | C#-Tecan | Java | Python |
| -------------------------- | ---- |---- | -------- | ---- | ------ |
|  Defined Execution Error   | x    | x   | x        | x    |      x |
|  Undefined Execution Error | x    | x   | x        | x    |      x |
|  Framework Error           | x    | x   | x        | x    |      x |

## Encryption

|                            | C++  | C#  | C#-Tecan | Java | Python |
| -------------------------- | ---- |---- | -------- | ---- | ------ |
|  Encrypted communication   | x    | x   | x        | x    |      x |
